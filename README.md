This repository contains supplementary materials for a DC paper titled 
"Scrutability of Intelligent Personal Assistants" by Jovan Jeromela 
published at the UMAP'22 conference. Please cite [1] when refering to 
this repository or any materials containted in it.

This prepository presents results of reanalysing data from two user 
studies, which were originally presented in [2] and [3]. Please look at 
the attached PDF file "Findings" for more details.

The dataset from [2] was received by contacting the corresponding author.
The dataset from [3] was downloaded with the author's permission from 
https://github.com/SarahTheres/Envisioned-Voice-Assistant-Dialogues. 
The dataset from [3] ought to be downloaded to run the overview notebook.

We thank the authors for their permission to access and analyse their 
datasets and to publically shared selected parts.


[1] Jovan Jeromela. 2022. Scrutability of Intelligent Personal Assistants. In Proceedings of the 30th ACM Conference on User Modeling, Adaptation and Personalization (UMAP ’22), July 4–7, 2022, Barcelona, Spain. ACM, New York, NY, USA, 6 pages. https://doi.org/10.1145/3503252.3534355


[2] Benjamin R. Cowan, Nadia Pantidi, David Coyle, Kellie Morrissey, Peter Clarke, Sara Al-Shehri, David Earley, and Natasha Bandeira. 2017. “What Can I Help You With?”: Infrequent Users’ Experiences of Intelligent Personal Assistants. Proceedings of the 19th International Conference on Human-Computer Interaction with Mobile Devices and Services, Article 43 pages. https://doi.org/10.1145/3098279.3098539


[3]  Sarah Theres Völkel, Daniel Buschek, Malin Eiband, Benjamin R. Cowan, and Heinrich Hussmann. 2021. Eliciting and Analysing Users’ Envisioned Dialogues with Perfect Voice Assistants. Association for Computing Machinery, Article 254. https://doi.org/10.1145/3411764.3445536
